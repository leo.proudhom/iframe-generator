const app = Vue.createApp({
  data() {
    return {
      domain: 'https://ns.europeancatalog.com/',
      width: '100%',
      scrolling: 'no',
      src: 'fr/',
      iframeCode: ''
    }
  },
  methods: {
    generateCode() {
      const code = `<iframe id="inlineFrameExample"
        width="${this.width}"
        height="2400"
        style="float:left"
        scrolling="${this.scrolling}"
        src="${this.domain}${this.src}"
      ></iframe>`
      this.iframeCode = code.trim()
    }
  }
})

app.mount('#app')
